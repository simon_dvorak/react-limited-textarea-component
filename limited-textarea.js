'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LimitedTextarea = function (_React$Component) {
  _inherits(LimitedTextarea, _React$Component);

  function LimitedTextarea(props) {
    _classCallCheck(this, LimitedTextarea);

    var _this = _possibleConstructorReturn(this, (LimitedTextarea.__proto__ || Object.getPrototypeOf(LimitedTextarea)).call(this, props));

    _this.parseWarnProp = function (warnProp, maxLength) {
      switch (true) {
        case warnProp === 'max':
        case warnProp === 'full':
          return maxLength;
          break;

        case warnProp.indexOf('%') != -1:
          return _this.percentageToChars(parseInt(warnProp), maxLength);
          break;

        case parseInt(warnProp) == warnProp:
          return parseInt(warnProp);
          break;

        case warnProp === 'never':
        default:
          return maxLength + 1;
          break;
      }
    };

    _this.percentageToChars = function (warnPercent, maxLength) {
      return maxLength * (warnPercent / 100);
    };

    _this.checkLength = function (text) {
      var currentLength = text.length;
      if (currentLength >= _this.state.warnLength) {
        var charsRemaining = _this.state.maxLength - text.length;
        return _this.setState({
          warnState: true,
          warnMessage: 'Warning: ' + charsRemaining + ' characters remaining.'
        });
      }

      return _this.setState({
        warnState: false,
        warnMessage: ''
      });
    };

    _this.state = {
      warnLength: _this.parseWarnProp(props.warn, props.maxLength),
      maxLength: props.maxLength || 100,
      warnState: false,
      warnMessage: ''
    };
    return _this;
  }

  _createClass(LimitedTextarea, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return React.createElement(
        'div',
        { className: this.props.containerClass },
        React.createElement('textarea', { onChange: function onChange(e) {
            return _this2.checkLength(e.target.value);
          }, maxLength: this.state.maxLength }),
        this.state.warnState ? React.createElement(
          'div',
          { className: this.props.warnClass },
          this.state.warnMessage
        ) : React.createElement('div', null)
      );
    }
  }]);

  return LimitedTextarea;
}(React.Component);
// Mount component into DOM elements


var components = document.getElementsByTagName('LimitedTextarea');
var _iteratorNormalCompletion = true;
var _didIteratorError = false;
var _iteratorError = undefined;

try {
  for (var _iterator = components[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
    var component = _step.value;
    var _component$attributes = component.attributes,
        maxLength = _component$attributes.maxLength,
        warn = _component$attributes.warn,
        containerClass = _component$attributes.containerClass,
        warnClass = _component$attributes.warnClass;

    ReactDOM.render(React.createElement(LimitedTextarea, { containerClass: containerClass.value, warnClass: warnClass.value, warn: warn.value, maxLength: maxLength.value }), component);
  }
} catch (err) {
  _didIteratorError = true;
  _iteratorError = err;
} finally {
  try {
    if (!_iteratorNormalCompletion && _iterator.return) {
      _iterator.return();
    }
  } finally {
    if (_didIteratorError) {
      throw _iteratorError;
    }
  }
}