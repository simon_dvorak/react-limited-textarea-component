'use strict';

class LimitedTextarea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      warnLength: this.parseWarnProp(props.warn, props.maxLength),
      maxLength: props.maxLength || 100,
      warnState: false,
      warnMessage: '',
    };
  }

  parseWarnProp = (warnProp, maxLength) => {
    switch (true){
      case warnProp === 'max':
      case warnProp === 'full':
        return maxLength;
      break;

      case warnProp.indexOf('%') != -1:
        return this.percentageToChars(parseInt(warnProp), maxLength);
      break;

      case parseInt(warnProp) == warnProp:
        return parseInt(warnProp);
      break;

      case warnProp === 'never':
      default:
        return maxLength + 1;
      break;
    }
  }

  percentageToChars = (warnPercent, maxLength) => {
    return maxLength * (warnPercent / 100);
  }

  checkLength = (text) => {
    const currentLength = text.length;
    if (currentLength >= this.state.warnLength){
      const charsRemaining = this.state.maxLength - text.length;
      return this.setState({
        warnState: true,
        warnMessage: `Warning: ${charsRemaining} characters remaining.`
      });
    }

    return this.setState({
      warnState: false,
      warnMessage: '',
    });

  }

  render() {
    return (
      <div className = {this.props.containerClass}>
        <textarea onChange = { e => this.checkLength(e.target.value) } maxLength = {this.state.maxLength}></textarea>
        {this.state.warnState ? <div className = {this.props.warnClass}>{this.state.warnMessage}</div> : <div />}
      </div>
    );
  }
}
// Mount component into DOM elements
let components = document.getElementsByTagName('LimitedTextarea');
for (let component of components){
  const {maxLength, warn, containerClass, warnClass} = component.attributes;
  ReactDOM.render( <LimitedTextarea containerClass = {containerClass.value} warnClass = {warnClass.value} warn = {warn.value} maxLength = {maxLength.value} />, component );
}